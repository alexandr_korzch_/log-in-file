package ua.com.pinta.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("tag", "hello");


        Logger LOG = (Logger) LoggerFactory.getLogger(MainActivity.class);
        
        LOG.info("info hello world");
        LOG.debug("reply: {}", "hello_reply");

    }

}
